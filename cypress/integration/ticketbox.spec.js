describe("Ticketbox", () => {
  beforeEach(() => cy.visit("index.html"));

  context("Personal info", () => {
    const personalInfo = {
      firstName: "João",
      lastName: "Silva",
      email: "joaosilva@example.com"
    };

    it("fills fullname and email", () => {
      cy.get("#first-name").type(personalInfo.firstName);
      cy.get("#last-name").type(personalInfo.lastName);
      cy.get("#email").type(personalInfo.email);
    });

    describe("Using support command", () => {
      it("fills fullname and email", () => {
        cy.fillPersonalInfo(personalInfo);
      });

      it("fills fullname and email slowly", () => {
        cy.fillPersonalInfoSlowly(personalInfo);
      });

      it("fills fullname and email quickly", () => {
        cy.fillPersonalInfoQuickly(personalInfo);
      });

      it("fills only firstname and email", () => {
        cy.fillPersonalInfo({
          firstName: personalInfo.firstName,
          email: personalInfo.email
        });
      });

      it("fills only lastname and email", () => {
        cy.fillPersonalInfo({
          lastName: personalInfo.lastName,
          email: personalInfo.email
        });
      });
    });
  });

  context("Ticket info", () => {
    context("Using support command", () => {
      it("selects 4 VIP tickets, social media, and some special request", () => {
        const ticketInfo = {
          quantity: 4,
          type: "VIP",
          howDidYouKnew: ["social-media"],
          specialRequest: "Vegan"
        };

        cy.fillTicketInfo(ticketInfo);
      });

      it("selects only a VIP ticket", () => {
        cy.fillTicketInfo({type: 'vip'});
      });

      it("selects two general tickets", () => {
        cy.fillTicketInfo({quantity: '2'});
      });
    });
  });

  context("Agreement", () => {
    context("Using support command", () => {
      it("accepts and signs the agreement", () => {
        cy.acceptAndSignAgreement("Walmyr Lima e Silva Filho");
      });

      it("accepts and signs the agreement with default signature", () => {
        cy.acceptAndSignAgreement();
      });
    });
  });
});