Cypress.Commands.add("acceptAndSignAgreement", (signature = "John Doe") => {
  cy.get("#agree").check();
  cy.get("#signature").type(signature);
});