Cypress.Commands.add("fillTicketInfo", info => {
  if (info.quantity) {
    let quantity = info.quantity;

    if (typeof(quantity === "number")) {
      quantity = quantity.toString();
    }

    cy.get("#ticket-quantity").select(quantity);
  }
  
  if (info.type) {
    const type = info.type.toLowerCase();

    if (type === "general") {
      cy.get("#general").check();
    }

    if (type === "vip") {
      cy.get("#vip").check();
    }
  }

  if (info.howDidYouKnew) {
    info.howDidYouKnew.forEach(option => {
      if (option === "friend") {
        cy.get("#friend").check();
      }
  
      if (option === "publication") {
        cy.get("#publication").check();
      }
  
      if (option === "social-media") {
        cy.get("#social-media").check();
      }
    });
  }

  if (info.specialRequest) {
    cy.get("#requests").type(info.specialRequest);
  }
});