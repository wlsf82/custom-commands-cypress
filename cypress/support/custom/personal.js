Cypress.Commands.add("fillPersonalInfoSlowly", (info, delay = 100) => {
  cy.fillPersonalInfo(info, delay);
});

Cypress.Commands.add("fillPersonalInfoQuickly", (info, delay = 1) => {
  cy.fillPersonalInfo(info, delay);
});

Cypress.Commands.add("fillPersonalInfo", (info, DEFAULT_DELAY = 10) => {
  if (info.firstName) {
    cy.get("#first-name").type(info.firstName, { delay: DEFAULT_DELAY });
  }
  
  if (info.lastName) {
    cy.get("#last-name").type(info.lastName, { delay: DEFAULT_DELAY });
  }
  
  if (info.email) {
    cy.get("#email").type(info.email, { delay: DEFAULT_DELAY });
  }
});
